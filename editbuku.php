<?php
include "includes/header.php";
include "includes/config.php";
?>

<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4">Form Buku</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Home</li>
            <li class="breadcrumb-item active">Form Buku</li>
        </ol>
        <?php
            $query = mysqli_query($koneksi, "SELECT * FROM data_buku where kode_buku = '".$_GET['kode']."'");
            $data = mysqli_fetch_array($query);
        ?>
        <form action="" method="post" enctype="multipart/form-data">
        <div class="form-group row">
                <label for="kd_buku" class="col-sm-2 col-form-label">Kode Buku</label>
                <div class="col-sm-10">
                    <input type="text" name="kd_buku" id="kd_buku" class="form-control" placeholder="Kode Buku" value="<?php echo $data['kode_buku'];?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="j_buku" class="col-sm-2 col-form-label">Judul Buku</label>
                <div class="col-sm-10">
                    <input type="text" name="j_buku" id="j_buku" class="form-control" placeholder="Judul Buku" value="<?php echo $data['j_buku'];?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="pengarang" class="col-sm-2 col-form-label">Pengarang</label>
                <div class="col-sm-10">
                    <input type="text" name="pengarang" id="pengarang" class="form-control" placeholder="Pengarang" value="<?php echo $data['pengarang'];?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
                <div class="col-sm-10">
                    <input type="text" name="penerbit" id="penerbit" class="form-control" placeholder="Penerbit" value="<?php echo $data['penerbit'];?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="th_terbit" class="col-sm-2 col-form-label">Tahun Terbit</label>
                <div class="col-sm-10">
                    <input type="text" name="th_terbit" id="th_terbit" class="form-control" placeholder="Tahun Terbit" value="<?php echo $data['th_terbit'];?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="exemplar" class="col-sm-2 col-form-label">Exemplar</label>
                <div class="col-sm-10">
                    <input type="text" name="exemplar" id="exemplar" class="form-control" placeholder="Jumlah Exemplar" value="<?php echo $data['exemplar'];?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="klasifikasi" class="col-sm-2 col-form-label">Klasifikasi</label>
                <div class="col-sm-10">
                    <input type="text" name="klasifikasi" id="klasifikasir" class="form-control" placeholder="Kode Klasifikasi" value="<?php echo $data['klasifikasi'];?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="asal_buku" class="col-sm-2 col-form-label">Asal Buku</label>
                <div class="col-sm-10">
                    <input type="text" name="asal_buku" id="asal_buku" class="form-control" placeholder="Asal Buku" value="<?php echo $data['asal_buku'];?>">
                    <small id="h_buku" class="text-muted">*Optional</small>
                </div>
            </div>
            <div class="form-group row">
                <label for="h_buku" class="col-sm-2 col-form-label">Harga Buku</label>
                <div class="col-sm-10">
                    <input type="text" name="h_buku" id="h_buku" class="form-control" placeholder="Harga Buku" aria-describedby="h_buku" value="<?php echo $data['h_buku'];?>">
                    <small id="h_buku" class="text-muted">*Optional</small>
                </div>
            </div>
            <div class="form-group row">
                <label for="k_buku" class="col-sm-2 col-form-label">Kondisi Buku</label>
                <div class="col-sm-10">
                    <select class="form-control" id="k_buku" name="k_buku">
                        <option selected>Kondisi Buku</option>
                        <option value="Masih Bagus" <?php echo $data['k_buku'] == 'Masih Bagus' ? 'selected="selected"' : ''?>>Masih Bagus</option>
                        <option value="Sebagian Halaman Hilang" <?php echo $data['k_buku'] == 'Sebagian Halaman Hilang' ? 'selected="selected"' : ''?>>Sebagian Halaman Hilang</option>
                        <option value="Rusak" <?php echo $data['k_buku'] == 'Rusak' ? 'selected="selected"' : ''?>>Rusak</option>
                    </select>
                    <small id="h_buku" class="text-muted">*Optional</small>
                </div>
            </div>
            <div class="form-group row">
                <label for="sinopsis" class="col-sm-2 col-form-label">Sinopsis</label>
                <div class="col-sm-10">
                    <textarea id="sinopsis" class="form-control" name="sinopsis" rows="3" value="<?php echo $data['sinopsis'];?>"></textarea>
                    <small id="h_buku" class="text-muted">*Optional</small>
                </div>
            </div>
            <button type="submit" name="simpan" class="btn btn-success float-right ml-2">Simpan</button>
            <button type="button" class="btn btn-danger float-right ml-2" onclick="kembali()">Kembali</button>
        </form>
    </div>
</main>

<script>
    function kembali() {
        location.href = "masterbuku.php";
    }
</script>

<?php
include "includes/footer.php";
?>