<?php
include "includes/header.php";
include "includes/config.php";
?>

<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4">Data Koleksi</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Home</li>
        </ol>
        <div class="row" style="padding-bottom: 35px;">
            <div class="col-xl-5 col-md-6">
                <form method="POST" action="">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Input Kode Buku/Judul">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xl-5 col-md-6">
                <button type="button" class="btn btn-info float-right ml-8" onclick="tambahbuku()">Tambah Buku</button>
            </div>
        </div>
        <table style="padding: top 25px;" class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Penerbit</th>
                    <th class="text-center">Pengarang</th>
                    <th class="text-center">Klasifikasi</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <?php
            $query = mysqli_query($koneksi, "SELECT * FROM data_buku");
            while ($data = mysqli_fetch_array($query)) {
            ?>
                <tbody>
                    <tr>
                        <td class="text-center"><?php echo $data['kode_buku'] ?></td>
                        <td class="text-center"><?php echo $data['j_buku'] ?></td>
                        <td class="text-center"><?php echo $data['penerbit'] ?></td>
                        <td class="text-center"><?php echo $data['pengarang'] ?></td>
                        <td class="text-center"><?php echo $data['klasifikasi'] ?></td>
                        <td class="text-center">
                            <a href="editbuku.php?kode=<?php echo $data['kode_buku']; ?>">
                                <input type="button" class="btn btn-success" value="Edit">
                            </a>
                            <a href="#?kode=<?php echo $data['kode_buku']; ?>">
                                <input type="button" class="btn btn-danger" value="Hapus">
                            </a>
                        </td>
                    </tr>
                </tbody>
            <?php } ?>
            <tfoot>
                <tr>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Penerbit</th>
                    <th class="text-center">Pengarang</th>
                    <th class="text-center">Klasifikasi</th>
                    <th class="text-center">Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
</main>

<script>
    function tambahbuku() {
        location.href = "formbuku.php";
    }
</script>

<?php
include "includes/footer.php";
?>