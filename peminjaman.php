<?php
include "includes/header.php";
?>

<script>
    function addBuku(buku) {
        var kode, judul, pengarang, jumlah;
        kode = document.getElementById(+buku).innerHTML;
        judul = document.getElementById(+buku).innerHTML;
        pengarang = document.getElementById(+buku).innerHTML;
        jumlah = document.getElementById(+buku).innerHTML;

        document.getElementById('').value = kode;
        document.getElementById('').value = judul;
        document.getElementById('').value = pengarang;
        document.getElementById('').value = jumlah;
    }
</script>

<main>
    <div class="container px-4">
        <div class="modal fade" id="m_buku">
            <div class="modal-dialog modal-m">
                <div class="modal-header">
                    <h4 class="modal-title">Data Buku</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table class="table table-hover" id='m_buku'>
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>#</th>
                                <th>#</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td><button type="button" class="btn btn-info" data-dismiss="modal" onclick="addBuku(buku)"></button></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>#</th>
                                <th>#</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid px-4">
        <h1 class="mt-4">Peminjaman</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Home</li>
        </ol>
        <form action="" method="POST">
            <div class=row>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="tanggan">Tanggal Peminjaman: </label>
                        <?php
                        date_default_timezone_set("Asia/Bangkok");
                        ?>
                        <input type="date" class="form-control" id="tanggal" placeholder="Tanggal" name="tanggal" value=<?php echo date('Y-m-d'); ?> required>
                    </div>
                </div>
                <div class="col-sm-3"></div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label for="kode">Nama</label>
                        <input type="text" class="form-control" id="namakonsumen" placeholder="Enter nama" name="nama" require>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="kode">Telp :</label>
                        <input type="text" class="form-control" id="telp" placeholder="Enter Telp" name="telp" required>
                    </div>
                </div>
                <div class="col-sm-3"></div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label for="keterangan">Keterangan:</label>
                        <input type="text" class="form-control" id="keterangan" placeholder="Enter Keterangan" name="keterangan" required>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="kode">Alamat:</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Enter Alamat" name="alamat" required>
                    </div>
                </div>
            </div>
        </form>
    </div>
</main>

<?php
include "includes/footer.php";
?>